/* Global Useful Configurations */
var pass = '123456';
var socketPort = '8089';
var socketDomain = 'sip.vocalcom-latam.com';
var ringtone = new Sound('./assets/snd/ringtone.mp3',100,true);
var remoteMedia;
var softphone;
var callSession;
var options = { 'mediaConstraints' : { 'audio': true, 'video': false } };

function createUAInstance(){

	var user = tbuser.value;
	var socket = new JsSIP.WebSocketInterface('wss://' + socketDomain + ':' + socketPort + '/ws');
	var configuration = {
		sockets  : [ socket ],
		uri      : 'sip:' + user + '@' + socketDomain,
		password : pass,
	};

	softphone = new JsSIP.UA(configuration);

	setSoftPhoneEventHandlers();			
	remoteMedia = document.getElementById('vid');
	softphone.start();

}


function setSoftPhoneEventHandlers(){

	
	softphone.on('connected', function(e){
		console.log("SoftPhone Conectado")
	})
	
	softphone.on('registered', function(){
		pnlLoginMenu.style.display='none';
		pnlSoftphone.style.display='block';
	})

	
	softphone.on('disconnected', function(e){
		console.log("Usuario " + user + " Desconectado.");
		pnlLoginMenu.style.display='block';
		pnlSoftphone.style.display='none';
	})
	
	
	
	softphone.on('newRTCSession', function(event){ 
		callSession = event.session; 
		setRTCSessionEventHandlers(callSession);
	})
}




// Vamos a definir todos los eventos con un console log, mas alla de que se usen
function setRTCSessionEventHandlers(NewRTC){
	
	NewRTC.on('peerconnection', function() {
		console.log('call is in peerconnection');
	})

	NewRTC.on('connecting', function() {
		console.log('call is connecting');
	})

	NewRTC.on('sending', function() {
		console.log('call is being sent');
	})

	NewRTC.on('progress', function() {
		console.log('call is in progress');
		console.log((NewRTC.direction == 'incoming') ? 'Recibiendo llamado' : 'Llamando');
		
		if(NewRTC.direction == 'incoming'){
			ringtone.start()
		}

		tbCallNumber.style.display='none'
		btnMakeCall.style.display='none';
		btnHangupCall.style.display='inline-block';

		if(NewRTC.direction == 'incoming'){
			btnAnswerCall.style.display='inline-block';
		}


	})
	
	NewRTC.on('accepted', function(){
		// Usar el onaddstream o el ontrack fue un fracaso si no queria definirlo en dos intancias distintas dependiendo de si la llamada era entrante o saliente
		// Mas facil agarrar el stream remoto en una instancia en la que ya lo tengo  en ambos puntos
		remoteMedia.srcObject = NewRTC.connection.getRemoteStreams()[0];
		(remoteMedia.srcObject) ? console.log('Escuchando media de extremo remoto') : 'No se encontro media de extremo remoto';
		
		if(NewRTC.direction == 'incoming'){
			ringtone.stop();
		}
		
		tbCallNumber.style.display='none'
		btnMakeCall.style.display='none';
		btnHangupCall.style.display='inline-block';
		btnAnswerCall.style.display='none';

	})

	NewRTC.on('confirmed', function() {
		console.log('call confirmed');
		
	})

	NewRTC.on('failed', function(data) {
		console.log('call failed with cause: '+ data.cause);
		
		if(NewRTC.direction == 'incoming'){
			ringtone.stop();
		}
		
		tbCallNumber.style.display='inline-block'
		btnMakeCall.style.display='inline-block';
		btnHangupCall.style.display='none';
		btnAnswerCall.style.display='none';
	
		remoteMedia.srcObject = null;
	})

	NewRTC.on('ended', function(data) {
		console.log('call ended with cause: '+ data.cause);
		
		tbCallNumber.style.display='inline-block'
		btnMakeCall.style.display='inline-block';
		btnHangupCall.style.display='none';
		btnAnswerCall.style.display='none';
		
		remoteMedia.srcObject = null;
	})

	// Fired before passing a remote SDP to the RTC engine, and before sending out a local SDP.
	// This event provides the mechanism to modify incoming and outgoing SDP.
	// Aunque pareciera que si, no se esta usando.
	NewRTC.on('sdp', function(data){
		callSessionDescription = new RTCSessionDescription({
			type: data.type,
			sdp: data.sdp	
		})
	})

	

}


function makeCall(){

	tbCallNumber.value;
	softphone.call("sip:" + tbCallNumber.value + "@"  + socketDomain,options);

}

function answerCall(){
	callSession.answer(options);
}

function hangupCall(){
	callSession.terminate();
}