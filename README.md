
# JsSip + Asterisk WebRTC

Prueba de concepto para la integracion de WebRTC sobre el res_pjsip de Asterisk, utilizando la libreria JsSip.
El mismo solo se basa en una interfaz reducida para la registracion de extensiones y su llamado entre ellas, con el fin de certificar que la configuracion de todas las partes que componen el proyecto fue exitosa y de ahi partir a las customizaciones que sean requeridas.

## Instrucciones de implementacion

Estas instrucciones tienen como fin dar una base sobre los puntos a configurar dentro del proyecto para  adecuarlo a los servidores que se vayan a utilizar de host.

### Requisitos Previos

El proyecto es dependiente de contar con los siguientes componentes:

```
Asterisk - 13 o superior con pjproject incluido
Server host - Se utiliza IIS
Certificados SSL para ambos servidores (los certificados self signed no son aptos para produccion)
```

### Instalacion

#### 1. Asterisk 
1. Reemplazar o agregar en el archivo http.conf

```
[general]
servername=<Identificador del Servidor Asterisk>
enabled=yes
bindaddr=0.0.0.0
bindport=8088
tlsenable=yes
tlsbindaddr=0.0.0.0:<Puerto a utilizar para registracion>
tlscertfile=<ruta absoluta del archivo .pem>
tlsprivatekey=<ruta absoluta del archivo .key>
```
2. Reemplazar o agregar en el archivo pjsip.conf
```
;Transporte para llamadas desde WSS
[<id de transporte>]
type=transport
bind=0.0.0.0
protocol=wss

;Configuracion la direccion dentro de Asterisk del contacto
[<id contacto>]
type=aor
max_contacts=1
remove_existing=yes

;Configuracion de autenticacion del usuario 1001
[<id contacto>]
type=auth
auth_type=userpass
username=1001
password=123456 ; Password insegura

;Configuracion del perfil de contacto
[<id contacto>]
type=endpoint
context=<contexto a utilizar en extensions.conf>
direct_media=no
allow=!all,alaw,ulaw,vp8,h264
aors=<id contacto aor>
auth=<id contacto auth>
max_audio_streams=10
max_video_streams=10
webrtc=yes
dtls_cert_file=<ruta absoluta del archivo .pem>
dtls_ca_file=<ruta absoluta del archivo .crt>

```
3. Opcional: Configurar identificacion de un host para recibir llamados de externos a WebRTC

Agregar la siguiente configuracion en el archivo pjsip.conf
```
;Transporte para llamadas desde Server remoto
[<id de transporte>]
type=transport
bind=0.0.0.0
protocol=<udp, tcp, tls o wss>

;Configuracion de endpoint para Server remoto
[<id de perfil>]
type=endpoint
transport=<id de transporte>
context=<contexto a utilizar en extensions.conf>
direct_media=no
allow=!all,alaw,ulaw

;Identificacion del Server remoto
[<id de identificador]
type=identify
endpoint=<id de endpoint destino>
match=<IPs o red a la que se le permite la identificacion>

```

#### 2.  Pagina Web
1. Modificar el archivo `.js\webrtc.js`

```
2. var socketPort = '<puerto configurado en http.conf';
3. var socketDomain = '<dominio con el que es reconocido Asterisk>';
```
4. Copiar carpetas al Servidor Web

```
./assets
./css
./js
index.html
```
5. Opcional: Modificar ringtone de llamada entrante
Reemplazar archivo `./assets/snd/ringtone.mp3` por el ringtone deseado, manteniendo el mismo nombre de archivo.


## Desarrollado con

* [JsSip 3.3.6](https://jssip.net/documentation/3.3.x/) - Libreria SIP para Javascript
* [Asterisk 15](https://wiki.asterisk.org/wiki/display/AST/Configuring+Asterisk+for+WebRTC+Clients) - Software de comunicacion OpenSource
* [Bootstrap 4](https://getbootstrap.com/) - Libreria de diseño web
